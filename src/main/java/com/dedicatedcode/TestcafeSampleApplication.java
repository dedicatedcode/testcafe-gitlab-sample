package com.dedicatedcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestcafeSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestcafeSampleApplication.class, args);
	}
}
