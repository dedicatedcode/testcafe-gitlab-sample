package com.dedicatedcode.testcafesample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by daniel on 02.03.17
 * (c) 2017 Daniel Wasilew <daniel@dedicatedcode.com>
 */
@Controller
public class ToDoController {
    private final List<ToDo> toDos = new ArrayList<>();

    public ToDoController() {
        toDos.add(new ToDo("write test cafe sample application"));
        toDos.add(new ToDo("build gitlab.ci configuration"));
        toDos.add(new ToDo("push to gitlab and watch ci runner"));
    }

    @RequestMapping("/")
    public String showToDos(ModelMap modelMap) {
        modelMap.addAttribute("todos", toDos);
        return "index";
    }

    @RequestMapping(value = "todo/new", method = RequestMethod.POST)
    public String addTodo(@RequestParam(name = "name") String name) {
        this.toDos.add(new ToDo(name));
        return "redirect:/";
    }

    @RequestMapping(value = "todo/delete", method = RequestMethod.POST)
    public String addTodo(@RequestParam(name = "id") UUID id) {
        ToDo toDelete = null;
        for (ToDo toDo : toDos) {
            if (toDo.getId().equals(id)) {
                toDelete = toDo;
            }
        }
        this.toDos.remove(toDelete);
        return "redirect:/";
    }
}
