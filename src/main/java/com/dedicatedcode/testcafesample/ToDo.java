package com.dedicatedcode.testcafesample;

import java.util.UUID;

/**
 * Created by daniel on 02.03.17
 * (c) 2017 Daniel Wasilew <daniel@dedicatedcode.com>
 */
public class ToDo {
    private final String task;
    private final UUID id;

    public ToDo(String task) {
        this.id = UUID.randomUUID();
        this.task = task;
    }

    public UUID getId() {
        return id;
    }

    public String getTask() {
        return task;
    }
}
