import {Selector} from "testcafe";
import {Index} from "../pages/StartPage";

fixture `Sample Test`
    .page `http://localhost:8080/`;

const index = new Index();

test('should create new todo', async t => {

    await index.addTodo("test todo");

    let location = await t.eval(() => window.location);
    await t.expect(location.pathname).eql('/');

    await index.containsTodo("test todo");
});

test('should delete todo', async t => {

    await index.addTodo("to delete todo");
    await index.containsTodo("to delete todo");
    await index.deleteTodo("to delete todo");
    await index.containsTodoNot("to delete todo");
});
